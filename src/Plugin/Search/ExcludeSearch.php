<?php

/**
 * @file
 * Contains \Drupal\search_exclude\Plugin\Search\ExcludeSearch.
 */

namespace Drupal\search_exclude\Plugin\Search;

use Drupal\node\NodeInterface;
use Drupal\Core\Access\AccessibleInterface;
use Drupal\search\Plugin\SearchIndexingInterface;

/**
 * Handles searching for node entities using the Search module index but with
 * the option to exclude certain content types.
 *
 * @SearchPlugin(
 *   id = "exclude_search",
 *   title = @Translation("Exclude search")
 * )
 */
class ExcludeSearch extends \Drupal\node\Plugin\Search\NodeSearch implements AccessibleInterface, SearchIndexingInterface {

  protected $excludes_types = array('page');

  /**
   * Indexes a single node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to index.
   */
  protected function indexNode(NodeInterface $node) {
    $type = $node->getType();
    if (!in_array($type, $this->excludes_types)) {
      parent::indexNode($node);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function indexStatus() {
    $total = $this->database->query('SELECT COUNT(*) FROM {node} WHERE type NOT IN (:excludes[])', array(':excludes[]' => $this->excludes_types))->fetchField();
    $remaining = $this->database->query("SELECT COUNT(DISTINCT n.nid) FROM {node} n LEFT JOIN {search_dataset} sd ON sd.sid = n.nid AND sd.type = :type WHERE sd.sid IS NULL OR sd.reindex <> 0 AND n.type NOT IN (:excludes[])", array(':type' => $this->getPluginId(), ':excludes[]' => $this->excludes_types))->fetchField();

    return array('remaining' => $remaining, 'total' => $total);
  }
}
