<?php
/**
 * @file
 * Contains \Drupal\search_exclude\Routing\RouteSubscriber.
 */

namespace Drupal\search_exclude\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('search.view_node_search')) {
      $route->setRequirement('_access', 'FALSE');
    }

    if ($route = $collection->get('search.view_exclude_search')) {
      $route->setPath('search/node');
    }
  }
}
